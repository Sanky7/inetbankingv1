package com.inetBanking.testCases;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.inetBanking.pageObjects.AddCustomerPage;
import com.inetBanking.pageObjects.LoginPage;

import junit.framework.Assert;
import net.bytebuddy.utility.RandomString;

public class TC_AddCustomer_003 extends BaseClass {
	
	@Test
	public void addNewCustomer() throws IOException
	{
		LoginPage lp = new LoginPage(driver);
		lp.setUserName(usrename);
		logger.info("username provided");
		lp.setPassword(password);
		logger.info("password provided");
		lp.clickSubmit();
		logger.info("Clicked on submit button");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		AddCustomerPage addcust = new AddCustomerPage(driver);
		addcust.clickAddNewCustomer();
		logger.info("Clicked on Add new Customer button");
		addcust.custName("Sanket");
		addcust.custGender("male");
		addcust.custDOB("27", "07", "87");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		addcust.custAddress("India");
		addcust.custCity("Navi Mumbai");
		addcust.custState("Maharashtra");
		addcust.custPin("410210");
		addcust.custTelphone("9876543210");
		
		
		
		String randomEamil = randomString() + "@gmail.com";
		addcust.custEmail(randomEamil);
		addcust.custPassword("abcdefg");
		
		logger.info("Providing the customer details");
		addcust.custSubmitBtn();
		
		logger.info("Validation Started");
		boolean result = driver.getPageSource().contains("Customer Registered Successfully!!!");
		
		
		if(result ==true)
		{
			Assert.assertTrue(true);
			logger.info("Test Case is Passed!");
		}
		else
		{
			captureScreen(driver, "TC_AddCustomer_003");
			Assert.assertTrue(false);
			logger.info("Test Case is Failed!");
		}
		
	}
	
}
