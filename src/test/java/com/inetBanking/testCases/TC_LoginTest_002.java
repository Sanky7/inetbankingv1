package com.inetBanking.testCases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.xml.XmlUtils;

import com.inetBanking.pageObjects.LoginPage;
import com.inetBanking.utilities.XLUtils;

public class TC_LoginTest_002 extends BaseClass
{
	@Test(dataProvider="LoginData")
	public void loginDDT(String user, String pwd)
	{
		LoginPage lp = new LoginPage(driver);
		lp.setUserName(user);
		logger.info("username provided");
		lp.setPassword(pwd);
		logger.info("password provided");
		lp.clickSubmit();
		logger.info("Clicked on login button!");
		
		if(isAlertPresent() == true)
		{
			driver.switchTo().alert().accept();
			driver.switchTo().defaultContent();
			Assert.assertTrue(false);
			logger.info("Login Failed!");
		}
		else
		{
			Assert.assertTrue(true);
			logger.info("Login Passed!");
			lp.clickLogout();
			driver.switchTo().alert().accept();
			driver.switchTo().defaultContent();
		}
	}
	
	
	public boolean isAlertPresent()
	{
		try 
			{
			driver.switchTo().alert();
			return true;
			}
		catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		
	}
	@DataProvider(name="LoginData")
	String[][] getData() throws IOException
	{
		String path = System.getProperty("user.dir")+"/src/test/java/com/inetBanking/testData/LoginCreds.xlsx";
		int rownNum = XLUtils.getRowCount(path, "sheet1");
		int colCount =XLUtils.getCellCount(path, "sheet1", 1);
		
		String logindata[][] = new String[rownNum][colCount];
		for(int i=1; i<=rownNum; i++)
		{
			for(int j=0; j<colCount; j++)
			{
				logindata[i-1][j] = XLUtils.getCellData(path, "sheet1", i, j); //1  0
			}
		}
		return logindata;
	}
}
