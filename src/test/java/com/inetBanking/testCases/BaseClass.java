package com.inetBanking.testCases;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.inetBanking.utilities.ReadConfig;

public class BaseClass {
	
	ReadConfig readconfig = new ReadConfig();
	
	public String baseURL = readconfig.getAppURL();
	public String usrename = readconfig.getUsrename();
	public String password = readconfig.getPassword();
	public static WebDriver driver;
	public static Logger logger;
	
	
	@Parameters("browser")
	@BeforeClass
	public void setup(String browser) {
		
		logger =logger.getLogger("eBanking");
		PropertyConfigurator.configure("log4j.properties");
		
		if(browser.equals("chrome"))
		{
		System.setProperty("webdriver.chrome.driver", readconfig.getChromeDriverPath());
		driver = new ChromeDriver();
		}
		else if(browser.equals("firefox")) 
		{	
			System.out.println("firefox browser in");
			System.setProperty("webdriver.gecko.driver", readconfig.getFirfoxDriverPath());
			driver = new FirefoxDriver();
		}
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(baseURL);
	}
	@AfterClass
	public void tearDown() {
		driver.quit();
	}
	
	public void captureScreen(WebDriver driver, String tname) throws IOException 
	{
		TakesScreenshot sc = (TakesScreenshot)driver;
		File src = sc.getScreenshotAs(OutputType.FILE);
		File target = new File(System.getProperty("user.dir")+"/ScreenShots/" + tname + ".png");
		FileUtils.copyFile(src, target);
		System.out.println("ScreenShot Taken"); 
	
	}
	
	public static String randomString()
	{
		String randomString  =RandomStringUtils.randomAlphabetic(5);
		return randomString;
	}
	
	public static String randomNumeric()
	{
		String randomString  = RandomStringUtils.randomNumeric(5);
		return randomString;
	}
}
