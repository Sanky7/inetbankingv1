package com.inetBanking.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AddCustomerPage {
	
	WebDriver driver;
	public AddCustomerPage(WebDriver rdrivver) {
		// TODO Auto-generated constructor stub
		PageFactory.initElements(rdrivver, this);
	}
	
@FindBy(how= How.XPATH, using ="//a[contains(text(),'New Customer')]")
@CacheLookup
WebElement linkAddNewCustomer;

@FindBy(how= How.XPATH, using ="//input[@name='name']")
@CacheLookup
WebElement txtCustomerName;

@FindBy(how= How.XPATH, using ="//input[@value='m']")
@CacheLookup
WebElement radGender;

@FindBy(how= How.XPATH, using ="//input[@id='dob']")
@CacheLookup
WebElement txtdob;

@FindBy(how= How.XPATH, using ="//textarea[@name='addr']")
@CacheLookup
WebElement txtAddress;

@FindBy(how= How.XPATH, using ="//input[@name='city']")
@CacheLookup
WebElement txtCity;

@FindBy(how= How.XPATH, using ="//input[@name='state']")
@CacheLookup
WebElement txtState;

@FindBy(how= How.XPATH, using ="//input[@name='pinno']")
@CacheLookup
WebElement txtPin;

@FindBy(how= How.XPATH, using ="//input[@name='telephoneno']")
@CacheLookup
WebElement txtTelephoneno;

@FindBy(how= How.XPATH, using ="//input[@name='emailid']")
@CacheLookup
WebElement txtEmailid;

@FindBy(how= How.XPATH, using ="//input[@name='password']")
@CacheLookup
WebElement txtPassword;

@FindBy(how= How.XPATH, using ="//input[@value='Submit']")
@CacheLookup
WebElement btnSubmit;

@FindBy(how= How.XPATH, using ="//input[@value='Reset']")
@CacheLookup
WebElement btnReset;


	public void clickAddNewCustomer()
	{
		linkAddNewCustomer.click();
	}
	public void custName(String cname)
	{
		txtCustomerName.sendKeys(cname);
	}
	public void custGender(String cgender)
	{
		radGender.click();
	}
	public void custDOB(String mm, String dd, String yy)
	{
		txtdob.sendKeys(mm);
		txtdob.sendKeys(dd);
		txtdob.sendKeys(yy);
	}
	public void custAddress(String caddress)
	{
		txtAddress.sendKeys(caddress);
	}
	public void custCity(String city)
	{
		txtCity.sendKeys(city);
	}
	public void custState(String state)
	{
		txtState.sendKeys(state);
	}
	public void custPin(String pin)
	{
		txtPin.sendKeys(String.valueOf(pin));
	}
	public void custTelphone(String telnum)
	{
		txtTelephoneno.sendKeys(telnum);
	}
	public void custEmail(String email)
	{
		txtEmailid.sendKeys(email);
	}
	public void custPassword(String pwd)
	{
		txtPassword.sendKeys(pwd);
	}
	public void custSubmitBtn()
	{
		btnSubmit.click();
	}
}



